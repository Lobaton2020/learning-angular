import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exercise-sumar',
  templateUrl: './exercise-sumar.component.html',
  styleUrls: ['./exercise-sumar.component.css']
})

export class ExerciseSumarComponent implements OnInit {
  num1:number = 67;
  num2:number = 0;
  result:number = 0;

  constructor() {}

  ngOnInit(): void {}

  onSum(){
    this.result = this.num1 + this.num2
  }

}
