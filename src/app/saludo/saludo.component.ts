import { Component, OnInit } from '@angular/core';

@Component({
  selector: '#app-saludo',
  template: `
  <div>
    <h1>{{ message }}</h1>
    <p>{{ getText() }}</p>
    <h5>Subscribe </h5>
    <button (click)="share()">Shareme</button>
    <p id="{{ idComponent }}-sufijo">Holii</p>
    <p [id]="idComponent">jajaj</p>
    <input [type]="idInput">
  </div>`,
  styles: [`
    div{
      width:500px;
      box-shadow:0 0 4px gray;
      padding:7px;
      border-radius:10px
    }
    h1{
      color:red;
      font-family:Helveltica
    }
    h5{
      padding:10px;
      border-radius:10px;
      background:#ececec
    }
  `]
})
export class SaludoComponent implements OnInit {
  message:string;
  idComponent:string;
  idInput:string;
  constructor() {
    this.idInput ="text"
    this.idComponent ="mainas"
    this.message = "Hola a todos como estan"
    setTimeout(() => {
      this.idInput ="password"
      this.message = "Ha cambiado el mensaje"
    }, 3000);
   }

  share():void{
    alert("Compartir")
  }
  ngOnInit():void {
  }
  getText():string{
    return "Hola este es un texto desde el Controller"
  }

}
