import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {
  message:string = "";
  constructor() { }

  ngOnInit(): void {
  }

  onClick(e):void{
    console.log(e.target.outerHTML)
  }

  onChange(e:any):void{
    console.log(e.target.value)
  }

  onMouseOut(e:any):void{
    this.message = `${e.clientY} ${e.clientX}`
    console.log(e)
  }
  onMouseEnter(e:any):void{
    this.message = "Mouse dentro"
    console.log(e)
  }
}
