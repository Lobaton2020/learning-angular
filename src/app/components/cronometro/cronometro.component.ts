import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cronometro',
  templateUrl: './cronometro.component.html',
  styleUrls: ['./cronometro.component.css']
})
export class CronometroComponent implements OnInit {
  numCronometre :number =5
  constructor() { }

  ngOnInit(): void {
  }
  startCronometre(){
    let idInterval = setInterval(()=>{
      this.numCronometre--;
      if (this.numCronometre == 0){
          clearInterval(idInterval)
        }
    },1000)
  }
}
